# projectdoc Blueprints for Teamwork Model

## Overview

This project specifies a model to create a free [doctype add-on](https://www.smartics.eu/confluence/x/nQHJAw)
for the [projectdoc Toolbox](https://www.smartics.eu/confluence/x/1YEp)
for [Confluence](https://www.atlassian.com/software/confluence).

The add-on provides blueprints to define rules for collaboration, including

  * Checklist
  * Pattern
  * Pattern Domain
  * Project Rule
  * Team Process
  * Tool

## Fork me!
Feel free to fork this project to adjust the model according to your project
requirements.

The Teamwork Doctypes Model and it's derived artifacts are licensed under
[Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

## Documentation

For more information please visit

  * the [add-on's homepage](https://www.smartics.eu/confluence/x/BYO5)
  * [projectdoc Toolbox Online Manual](https://www.smartics.eu/confluence/x/EAFk)
  * [Doctype Maven Plugin](https://www.smartics.eu/confluence/x/zgDqAw)

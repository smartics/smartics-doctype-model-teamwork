<?xml version='1.0'?>
<!--

    Copyright 2014-2025 smartics, Kronseder & Reiner GmbH

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="checklist"
  base-template="standard"
  provide-type="standard-type"
  category="team">
  <resource-bundle>
    <l10n>
      <name>Checklist</name>
      <description>
        Document a checklist for executing a manual oder semi-automatic task.
      </description>
      <about>
        Create a checklist to support team members in executing regular tasks
        that have not (yet) been automated. Checklists allow to run manual
        tasks in a defined manner. It guides the user of the checklist through
        a process and helps to not forget a step. A checklist is a lightweight
        process defined by the team.
      </about>
    </l10n>
    <l10n locale="de">
      <name plural="Checklisten">Checkliste</name>
      <description>
        Dokumentieren Sie Arbeitsschritten, die nicht automatisiert
        abgearbeitet werden können.
      </description>
      <about>
        Checklisten helfen Ihnen bei der Durchführung wiederkehrender,
        manueller Schritte. Das Team definiert mit einer Checkliste einen
        leichtgewichtigen Prozess.
      </about>
      <type plural="Checklisten-Typen">Checklisten-Typ</type>
    </l10n>
  </resource-bundle>

  <properties>
    <property key="projectdoc.doctype.common.type">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">checklist-type</param>
          <param
            name="property"
            key="projectdoc.doctype.common.type" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
    </property>
    <property key="projectdoc.doctype.common.sponsors">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">stakeholder,organization,person,role</param>
          <param
              name="property"
              key="projectdoc.doctype.common.sponsors" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="empty-as-none">true</param>
        </macro>
      </value>
    </property>
  </properties>

  <sections>
    <section key="projectdoc.doctype.common.description">
      <config>
        <param name="show-title">false</param>
      </config>
      <resource-bundle>
        <l10n>
          <description>
            Describe the purpose of the checklist. This may include reasons why
            this has not (yet) been automated.
          </description>
        </l10n>
        <l10n locale="de">
          <description>
            Beschreiben Sie den Zweck dieser Checkliste. Dies kann eine
            Erklärung beinhalten, warum der hier beschriebene Vorgang noch
            nicht automatisiert wurde.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="projectdoc.doctype.checklist.context">
      <resource-bundle>
        <l10n>
          <name>Context</name>
          <description>
            Provide background information for the reader to understand the
            task that is accomplished by following the checklist.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Kontext</name>
          <description>
            Stellen Sie Hintergrundinformationen bereit, die für das
            Verständnis der Aufgabe, durch die die Checkliste führt, wichtig
            sind.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="projectdoc.doctype.checklist.prerequisites">
      <resource-bundle>
        <l10n>
          <name>Prerequisites</name>
          <description>
            Provide information about what has to be done before to start with
            the checklist.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Vorbedingungen</name>
          <description>
            Verweisen Sie auf Bedingungen, die erfüllt werden müssen, bevor die
            Checkliste angewendet werden kann.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="projectdoc.doctype.checklist.steps">
      <resource-bundle>
        <l10n>
          <name>Steps</name>
          <description>
            Describe the steps of the checklist. Each step is described in its
            own subsection. Consider to use the Steps Macro.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Schritte</name>
          <description>
            Listen Sie die einzelnen Schritte, die durch die Checkliste
            abgearbeitet werden. Jeder Schritt wird typischerweise in einem
            eigenen Abschnitt behandelt. Hilfreich ist gegebenenfalls das
            Schritte Makro.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="projectdoc.doctype.checklist.postrequisites">
      <resource-bundle>
        <l10n>
          <name>Postrequisites</name>
          <description>
            Provide information about what has to or can be done after
            finishing the steps of this checklist.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Nachbedingungen</name>
          <description>
            Beschreiben Sie die Bedingungen, die nach Abarbeitung der
            Checkliste gelten oder noch getan werden könnte.
          </description>
        </l10n>
      </resource-bundle>
    </section>
    <section key="projectdoc.doctype.checklist.result">
      <resource-bundle>
        <l10n>
          <name>Result</name>
          <description>
            Describes the result - if not obvious - the user of the checklist
            has received.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Ergebnis</name>
          <description>
            Beschreiben sie das Ergebnis, das durch die erfolgreiche
            Abarbeitung der Checkliste erreicht wird. Die Beschreibung ist
            hilfreich, wenn das Ergebnis nicht offensichtlich ist.
          </description>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="pattern" />
    <doctype-ref id="project-rule" />
    <doctype-ref id="team-process" />
    <doctype-ref id="tool" />
  </related-doctypes>
</doctype>
